#
# Cookbook:: chef-server
# Recipe:: python
#

if node['platforn'] == 'ubuntu'
  if node['platform_version'] == '16.04'
    python_runtime '3'
  else
    python_runtime '2'
  end
end

node['server']['pip'].each do |package, version|
  python_package package do
    version version
  end
end
