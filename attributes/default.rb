default['server']['pip'] = {
  # pkg_name: pkg_version
}

default['server']['chef-solo-repos-destination'] = '/opt/chef-solo'

default['server']['cron']['jobs'] = [
  {
    name: 'chef-solo-run',
    hour: '*',
    minute: '0',
    command: 'cd /opt/chef-solo/ && /opt/chef-solo/run.sh > /var/log/chef-solo.log',
    only_if: ::Dir.exist?('/opt/chef-solo'),
  },
]

default['server']['scripts'] = []
