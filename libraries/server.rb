class Chef
  class Recipe
    def get_script_path(destination)
      path = destination.split('/')
      path.shift
      path.pop
      x = '/'
      path.each do |e|
        x = x + e + '/'
      end
      Chef::Log.debug(x)
      return x
    end
  end
end
