name 'server'
maintainer 'Julien Levasseur'
maintainer_email 'contact@julienlevasseur.ca'
license 'MIT'
description 'Installs/Configures server'
long_description 'Installs/Configures server'
version '0.1.0'
chef_version '>= 12.14' if respond_to?(:chef_version)

issues_url 'https://gitlab.com/julienlevasseur/server/issues'
source_url 'https://gitlab.com/julienlevasseur/server'

depends 'poise-python'

support 'centos'