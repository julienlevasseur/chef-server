#
# Cookbook:: server
# Recipe:: default
#

include_recipe 'server::python'
package 'git'

# Get the chef-solo package to run it via cron
#directory node['server']['chef-solo-repos-destination']
#git node['server']['chef-solo-repos-destination'] do
#  repository 'git@bitbucket.org:julienlevasseur/chef-solo-aws-server.git'
#  revision 'master'
#  action :checkout
#  not_if { Chef::Config[:file_cache_path].include?('kitchen') }
#end

# Setup the cron job
node['server']['cron']['jobs'].each do |cronjob|
  cron cronjob['name'] do # ~FC022
    hour cronjob['hour']
    minute cronjob['minute']
    command cronjob['command']
    only_if { cronjob['only_if'] } if cronjob['only_if']
    not_if { node['virtualization']['system'] == 'docker' }
  end
end

directory '/opt/scripts'

node['server']['scripts'].each do |script|
  directory get_script_path(script['destination']) do
    recursive true
  end
  remote_file script['destination'] do
    source script['source']
    mode '0755'
  end
end

# Vim setup:
git '/opt/Config-files' do
  repository 'https://gitlab.com/julienlevasseur/Config-files.git'
  revision 'master'
  action :checkout
end

execute 'vim_setup' do
  cwd '/opt/Config-files'
  command '/opt/Config-files/setup-vim.sh'
  not_if { node['virtualization']['system'] == 'docker' }
  # not_if { tagged?("vim_setuped") }
end
tag("vim_setuped")